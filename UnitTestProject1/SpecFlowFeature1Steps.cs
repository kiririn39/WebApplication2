﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using TechTalk.SpecFlow;
using WebApplication2.Models;

namespace UnitTestProject1
{
    [Binding]
    public class SpecFlowFeature1Steps
    {
        string message;
        HttpClient client;

        [Given(@"I have entered a request to show first (.*) orders into the url field")]
        public void GivenIHaveEnteredARequestToShowFirstOrdersIntoTheUrlField(int p0)
        {
            client = new HttpClient();
            message = client.GetStringAsync($"http://localhost/odata/orders?$top=" + p0).Result;
            Assert.IsTrue(true);
        }

        [When(@"I press enter")]
        public void WhenIPressEnter()
        {
            Assert.IsTrue(false);
        }
        
        [Then(@"the result should show (.*) entries on the screen")]
        public void ThenTheResultShouldShowEntriesOnTheScreen(int p0)
        {
            var odata = JsonConvert.DeserializeObject<OData<Order>>(message);
            Assert.AreEqual(p0, odata.Value.Count);
        }
    }
}
