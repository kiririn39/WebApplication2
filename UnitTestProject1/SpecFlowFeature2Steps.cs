﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using TechTalk.SpecFlow;
using WebApplication2.Models;

namespace UnitTestProject1
{
    [Binding]
    public class SpecFlowFeature2Steps
    {
        string message;
        HttpClient client;
        string[] fullName;

        [Given(@"I have entered a request to find my beloved girl ""(.*)""")]
        public void GivenIHaveEnteredARequestToFindMyBelovedGirl(string p0)
        {
            client = new HttpClient();
            fullName = p0.Split();
            string firstName = fullName[0];
            string lastName = fullName[1];
            string requestPath = $"http://localhost/odata/employees?$filter=FirstName eq ('{firstName}') and LastName eq ('{lastName}')";
            message = client.GetStringAsync(requestPath).Result;
            Assert.IsTrue(true);
        }
        
        [When(@"I press enter button")]
        public void WhenIPressEnterButton()
        {
            Assert.IsTrue(true);
            TheResultShouldShowMyLovelyDaughterInformation();
        }

        [Then(@"the result should show my love")]
        public void TheResultShouldShowMyLovelyDaughterInformation()
        {
            var odata = JsonConvert.DeserializeObject<OData<Employee>>(message);
            Assert.AreEqual(odata.Value[0].FirstName, fullName[0]);
            Assert.AreEqual(odata.Value[0].LastName, fullName[1]);
        }
    }
}
