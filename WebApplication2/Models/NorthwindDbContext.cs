﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Models
{
    public class NorthwindDbContext : DbContext
    {
        public DbSet<Order> Orders { get; set; }
        public DbSet<Employee> Employees { get; set; }

        public NorthwindDbContext(DbContextOptions<NorthwindDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
