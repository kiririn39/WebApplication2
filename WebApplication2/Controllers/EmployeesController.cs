﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    //[ApiController]
    //[Route("api/[controller]")]
    public class EmployeesController : ControllerBase
    {
        NorthwindDbContext _DbContext;
        public EmployeesController(NorthwindDbContext context)
        {
            _DbContext = context;
        }

        //[HttpGet]
        [EnableQuery]
        public IEnumerable<Employee> Get()
        {
            return _DbContext.Employees.ToList();
        }
    }
}